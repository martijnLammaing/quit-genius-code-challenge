
module.exports.NEW_BLOCK_PARAMETERS_EMPTY = {
	title: "NEW_BLOCK_PARAMETERS_EMPTY",
	message: "Block construction parameters can not be empty",
	code: 10000
};

module.exports.BLOCK_DOES_NOT_CONTAIN_CORRECT_HASH = {
	title: "BLOCK_DOES_NOT_CONTAIN_CORRECT_HASH",
	message: "Block does not contain correct hash",
	code: 10001
};

module.exports.NO_MINE_INDEX_ZERO_POSSIBLE = {
	title: "NO_MINE_INDEX_ZERO_POSSIBLE",
	message: "A mine index of zero is not allowed",
	code: 10002
};

module.exports.TRANSACTIONS_NOT_10_ITEMS_LONG = {
	title: "TRANSACTIONS_NOT_10_ITEMS_LONG",
	message: "Transactions are not a list of 10 transactions long",
	code: 10003
};

module.exports.BLOCK_CHAIN_INTEGRITY_HAS_BEEN_COMPROMISED = {
	title: "BLOCK_CHAIN_INTEGRITY_HAS_BEEN_COMPROMISED",
	message: "Block chain integrity has been compromised",
	code: 10004
};