const ERRORS = require("./errors");
const AppError = require("./appError");

/*
	Calculated the expected hash the next block has to contain to get added to the chain
*/
const calcNextHash = (block) => {
	const firstValue = block._hash;
	const secondValue  = (block._previous) ? block._previous._hash : 0;

	return firstValue + secondValue;
};

/*
	Checks all the nodes in the chain if all the hashes are still correct, and have not been compromised
*/
const checkIntegrity = (block) => {

	if(block._previous !== null){
		if(block._hash !== calcNextHash(block._previous)){
			throw new AppError(ERRORS.BLOCK_CHAIN_INTEGRITY_HAS_BEEN_COMPROMISED);
		}

		checkIntegrity(block._previous);
	}
};



class Block {
	constructor(transactions, hash){
		if(!transactions || !hash){
			throw new AppError(ERRORS.NEW_BLOCK_PARAMETERS_EMPTY);
		}
		if(Object.keys(transactions).length !== 10){
			throw new AppError(ERRORS.TRANSACTIONS_NOT_10_ITEMS_LONG);
		}

		this._transactions = transactions;
		this._hash = hash;
		this._next = null;
		this._previous = null;
	}

	/*
		Adds new block to blockchain
		*Used array with transaction objects instead of an object dictionary because those demand for unique key values, which in this scenario is not possible...
	*/
	addNewBlock(transactions, hash) {

		if(hash !== calcNextHash(this)){
			throw new AppError(ERRORS.BLOCK_DOES_NOT_CONTAIN_CORRECT_HASH);
		}

		checkIntegrity(this);

		const newBlock = new Block(transactions, hash);

		this._next = newBlock;
		newBlock._previous = this;

		return newBlock;
	}
}


/*
	Returns the fibonacci number for the given index from the fibonacci series
*/
const mine = (index) => {
	if(index === 0){
		throw new AppError(ERRORS.NO_MINE_INDEX_ZERO_POSSIBLE);
	}

	let fibo = 0;
	let previousfibo = 1;
	let previousBeforeOne = 0;

	for(let i = 1;i < index; i++){
		fibo = previousBeforeOne + previousfibo;
		previousBeforeOne = previousfibo;
		previousfibo = fibo;
	}

	return fibo;
};


/*
	Gets all transactions for a given wallet from the blockchain
*/
const getTransactionsForWallet = (block, walletId) => {
	const isTransactionFromWallet = (transaction, walletId) => {
		return parseInt(Object.keys(transaction)[0]) === walletId;
	};

	const transactions = block._transactions.filter((transaction) => { return isTransactionFromWallet(transaction, walletId); });
	const previousBlockTransactions = (block._previous) ? getTransactionsForWallet(block._previous, walletId) : [];

	return previousBlockTransactions.concat(transactions);
};

/*
	Adds all transactions up
*/
const addUpTransactions = (transactions) => {
	return transactions.reduce((acc, transaction) => { return acc + Object.values(transaction)[0]; }, 0);
};

/*
	Gets transactions from blockchain for wallet and adds them up
*/
const getBalance = (block, walletId) => {
	const transactions = getTransactionsForWallet(block, walletId);
	
	return addUpTransactions(transactions);
};



module.exports = Block;
module.exports.mine = mine;
module.exports.getBalance = getBalance;