
class AppError extends Error {
	constructor(error){
		super(error.message);

		this._title = error.title;
		this._code = error.code;
		this._message = error.message;
	}
}

module.exports = AppError;