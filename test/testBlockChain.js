
const chai = require("chai");
const expect = chai.expect;
const Block = require("../app/block");
const ERRORS = require("../app/errors");


describe("BlockChain", () => {

	const transactions = {
		randomTransactions: [ 
			{ 1: 20 }, 
			{ 1: -15 },
			{ 21: 55 },
			{ 13: 34 },
			{ 1: -18 },
			{ 48: 86 },
			{ 1: 40 },
			{ 8: 2 },
			{ 7: 14 },
			{ 30: 95 }
		],
		not10Transactions: [
			{ 5: 15 },
			{ 6: 5 }
		]
	}
	const hashes = {
		firstHash: 1,
		secondHash: 1,
		thirdHash: 2,
		fourthHash: 3,
		fifthHash: 5,
		randomHash: 13
	}

	describe("mine", () => {
		it("should return the correct number of the fibonacci sequence", () => {
			const expectedMinedValue = 6765;

			const sut = Block.mine(20);

			expect(sut).to.equal(expectedMinedValue);
		});
		it("should throw error when index 0 is given", () => {
			try {
				const sut = Block.mine(0);
				should.not.exist(sut);
			} catch (e) {
				expect(e._code).to.equal(ERRORS.NO_MINE_INDEX_ZERO_POSSIBLE.code);
			}
		})
	})

	describe("newBlock", () => {
		it("should create a new block object", () => {
			const transactionList = transactions.randomTransactions;
			const hash = hashes.randomHash;

			const sut = new Block(transactionList, hash);

			expect(sut).to.have.property("_hash").and.equal(hash);
			expect(sut).to.have.property("_transactions").and.equal(transactionList);
			expect(sut).to.have.property("_next").and.equal(null);
			expect(sut).to.have.property("_previous").and.equal(null);
		});

		it("should throw exception when constructor params arent given", () => {

			try {
		        const sut = new Block();
		        should.not.exist(sut);
		      } catch (e) {
		        expect(e._code).to.equal(ERRORS.NEW_BLOCK_PARAMETERS_EMPTY.code);
		      }
		});

		it("should throw exception when transactions are not 10 items long", () => {

			try {
				const sut = new Block(transactions.not10Transactions, hashes.randomHash);
				should.not.exist(sut);
			} catch (e) {
				expect(e._code).to.equal(ERRORS.TRANSACTIONS_NOT_10_ITEMS_LONG.code);
			}
		})
	})
	describe("addNewBlock", () => {
		const genesisBlock = new Block(transactions.randomTransactions, hashes.firstHash);
		const thirdBlock = new Block(transactions.randomTransactions, hashes.thirdHash);

		it("should add new block to chain", () => {

			const sut = genesisBlock.addNewBlock(transactions.randomTransactions, hashes.secondHash);

			expect(sut._hash).to.equal(hashes.secondHash);
			expect(sut._transactions).to.equal(transactions.randomTransactions);
			expect(sut._next).to.equal(null);
			expect(sut._previous._hash).to.equal(hashes.firstHash);
			expect(sut._previous._next._hash).to.equal(hashes.secondHash);
		})

		it("should throw error when block with incorrect hash is added", () => {
			const randomNewBlock = new Block(transactions.randomTransactions, hashes.randomHash);

			try {
				const sut = thirdBlock.addNewBlock(transactions.randomTransactions, hashes.randomHash);
				should.not.exist(sut);
			} catch(e) {
				expect(e._code).to.equal(ERRORS.BLOCK_DOES_NOT_CONTAIN_CORRECT_HASH.code);
			}
		})

		it("should throw error when block integrity is compromised", () => {
			const second = genesisBlock.addNewBlock(transactions.randomTransactions, hashes.secondHash);
			const third = second.addNewBlock(transactions.randomTransactions, hashes.thirdHash);

			const alteredBlock = third._previous._previous._hash = 3;

			try {
				const sut = third.addNewBlock(transactions.randomTransactions, hashes.fourthHash);
				should.not.exist(sut);
			} catch(e) {
				expect(e._code).to.equal(ERRORS.BLOCK_CHAIN_INTEGRITY_HAS_BEEN_COMPROMISED.code);
			}
		})
	})

	describe("getBalance", () => {
		const genesisBlock = new Block(transactions.randomTransactions, hashes.firstHash);
		
		it("should return balance of given wallet id", () => {
			const block = genesisBlock.addNewBlock(transactions.randomTransactions, hashes.secondHash)
										.addNewBlock(transactions.randomTransactions, hashes.thirdHash)
										.addNewBlock(transactions.randomTransactions, hashes.fourthHash);
			const walletId = 1;
			const expectedBalance = 108;

			const sut = Block.getBalance(block, walletId);

			expect(sut).to.equal(expectedBalance);
		})
	})
})