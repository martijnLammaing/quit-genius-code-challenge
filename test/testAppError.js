const chai = require("chai");
const expect = chai.expect;
const AppError = require("../app/appError");
const ERRORS = require("../app/errors");

describe("AppError", () => {
	describe("newError", () => {
		it("should create new error with parameters", () => {
			const ERROR = ERRORS.NEW_BLOCK_PARAMETERS_EMPTY;

			const sut = new AppError(ERROR);

			expect(sut).to.have.property("_title").and.equal(ERROR.title);
			expect(sut).to.have.property("_message").and.equal(ERROR.message);
			expect(sut).to.have.property("_code").and.equal(ERROR.code);
		})
	})
})