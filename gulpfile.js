const gulp = require('gulp');
const mocha = require('gulp-mocha');
const jshint = require('gulp-jshint');
const stylish = require('jshint-stylish');

const folder = {
		src: './'
	};

const handleError = () => {
	this.emit('end');
}

gulp.task('lint', function() {
	return gulp.src(folder.src + 'app/**/*')
		.pipe(jshint())
		.pipe(jshint.reporter(stylish))
		.pipe(jshint.reporter('fail'))
});

gulp.task('unitTests', ['lint'], function() {
	return gulp.src(['tests/unitTests/**/*.js'], { read: false })
		.pipe(mocha({
			reporter: 'spec',
			recursive: true,
		}))
		.on("error", handleError);
});

gulp.task('watch', function() {
	gulp.watch(folder.src + 'app/**/*', ['unitTests']);
	gulp.watch(folder.src + 'tests/**/*', ['unitTests']);
	gulp.start('unitTests');
});

gulp.task('start', ['watch']);